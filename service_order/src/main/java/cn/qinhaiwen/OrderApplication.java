package cn.qinhaiwen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 用户的启动类
 * @EnableEurekaClient： 标记该应用是 Eureka客户端
 */
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
public class OrderApplication {
    public static void main( String[] args )
    {
       // new SpringApplicationBuilder(OrderApplication.class).web(true).run(args);
       SpringApplication.run(OrderApplication.class);
    }
    @Bean
    // 负载均衡
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
