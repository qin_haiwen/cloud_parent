package cn.qinhaiwen.controller;

import cn.qinihaiwen.domain.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OrderController {
    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getByIdFallback")
    @GetMapping ("/getById/{id}")
    public User getById(@PathVariable("id") Long id){
        //String url = "http://localhost:1030/getById/"+id;
        String url = "http://USER-SERVER/getById/"+id;
        User user = restTemplate.getForObject(url, User.class);
        System.out.println("用户返回user"+user.getUsername());
        return user;
    }
    public User getByIdFallback(@PathVariable("id") Long id){

        return new User(-1L,"","服务异常");
    }
}
