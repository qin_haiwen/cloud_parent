package cn.qinhaiwen.controller;

import cn.qinihaiwen.domain.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    //加载端口
    @Value("${server.port}")
    private int port;

    @GetMapping("/getById/{id}")
    public User getById(@PathVariable("id") Long id){
        return new User(id ,"qhw","秦" +port);
    }

}
