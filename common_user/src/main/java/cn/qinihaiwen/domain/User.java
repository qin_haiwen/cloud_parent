package cn.qinihaiwen.domain;

public class User {
    private Long id;
    private String username;
    private String intro;

    public User() {
    }

    public User(Long id, String username, String intro) {
        this.id = id;
        this.username = username;
        this.intro = intro;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
}
