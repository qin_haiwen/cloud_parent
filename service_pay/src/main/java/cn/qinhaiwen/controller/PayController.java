package cn.qinhaiwen.controller;

import cn.qinhaiwen.feign.client.UserFeginClient;
import cn.qinihaiwen.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PayController {
    @Autowired
    private UserFeginClient userFeginClient;

    @GetMapping("/getById/{id}")
    public User getById(@PathVariable("id") Long id){

        return  userFeginClient.getById(id);
    }
}
