package cn.qinhaiwen.feign.client;

import cn.qinihaiwen.domain.User;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class UserFeginFallbackFactory implements FallbackFactory<UserFeginClient>{
    @Override
    public UserFeginClient create(Throwable throwable) {
        return new UserFeginClient() {
            @Override
            public User getById(Long id) {
                return new User(-1L,"","服务异常");
            }
        };
    }
}
