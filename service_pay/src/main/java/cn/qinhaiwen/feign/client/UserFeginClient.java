package cn.qinhaiwen.feign.client;

import cn.qinihaiwen.domain.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "USER-SERVER",fallbackFactory =UserFeginFallbackFactory.class )
public interface UserFeginClient {
   @GetMapping("/getById/{id}")
    User getById(@PathVariable("id") Long id);


}
